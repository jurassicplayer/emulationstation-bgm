# EmulationStation BGM

A smooth background music experience for EmulationStation, targeted towards raspberry pi/RetroPie setups.

In order to make the whole process as simple as possible there is only ONE script you need to download and put on your device. This script will install pygame & psutil if needed, get the latest version of the EmulationStation python script, help manage configuration options from within RetroPie, install/uninstall the python script, and even remove pygame & psutil if you want. 

It is entirely possible to have EmulationStation BGM share a config file with other programs. Just make sure that the EmulationStation_BGM.sh correctly points to the config file location before installing and it will handle the adding and removal of the settings while leaving all other sections intact.

After downloading the python script, it will copy over the paths INSTALL_LOC, INI_LOC, and LOG_LOC so you never have to touch the python script. In theory, the only script you will ever need to touch to do anything with this project is solely the EmulationStation_BGM.sh.

Requires: Python 3.5+, python-pygame

## Installation
- `cd ~/RetroPie/retropiemenu`
- `wget -O EmulationStation_BGM.sh --progress=bar:force:noscroll --show-progress -q "https://gitlab.com/jurassicplayer/emulationstation-bgm/raw/master/EmulationStation_BGM.sh"`
- `chmod +x EmulationStation_BGM.sh` (if needed)
- `nano EmulationStation_BGM.sh` and customize install, config, and log locations (if desired)
- `./EmulationStation_BGM.sh` or run it from the RetroPie menu
- `"Install ES BGM"`

## Uninstallation
- `./EmulationStation_BGM.sh` or run it from the RetroPie menu
- `"Uninstall ES BGM"`

## Configuration
| Option | Description |
| ------ | ------ |
| music_dir | Folder containing the music to rotate through |
| init_song | Initial song to play (does not have to reside in the music_dir) |
| init_delay | Initial delay (ms) before playing music to avoid clashing with splashscreen sfx |
| main_loop_sleep | Main loop sleep time (ms), controls responsiveness of the script |
| max_volume | A float value that specifies the maximum volume to play the BGM |
| vol_step_size | A float value that controls the smoothness of transitional fades |
| fade_duration | The time (ms) it takes to fade in/out from 0 to max_volume |
| resume_delay | The delay (ms) after a flagged program completes before resuming music |
| reset_song | Boolean value that enable/disables restarting the current song upon resume |
| cpu_threshold | Specifically for non-omxplayer users. ** |
| logging | Boolean value that enable/disables logging  |

** ESBGM monitors the CPU usage of EmulationStation and will trigger a music fade out if the
process reaches above the cpu_threshold. This mainly occurs when EmulationStation is playing
video snaps without external players (using libvlc), but can occur while scrolling through
lists and other operations. To minimize false positives, monitor the emulationstation process
with htop over ssh and choose a threshold equal to or slightly below the median of CPU ranges
while ES is playing a video.

If you are using omxplayer, it may be preferred to set the threshold to 100% to prevent any 
random CPU spikes from causing any fade outs.

## Caveat
- Not all MP3s will work, YMMV (converting them to .ogg works as far as I know)
- For sharing a config file, it MUST be a valid config with sections for all keys.
- Config file comments will be erased on write.

## Video Previews
- [Installation Video](https://streamable.com/aqa2iq)
- [Demonstration Video](https://streamable.com/cy2xas)

## Screenshots
![Main Menu](https://i.imgur.com/ogCmt3E.png)
![Change Music Dir](https://i.imgur.com/nh5bsdp.png)