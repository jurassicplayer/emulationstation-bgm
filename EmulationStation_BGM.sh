#!/bin/bash

SCRIPT_LOC="/opt/esbgm/esbgm.py"
INI_LOC="/opt/esbgm/addon.ini"
LOG_LOC="/dev/shm/esbgm.log"

INFO_DELAY=3
ERROR_DELAY=5

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
INSTALL_DIR=$(dirname "${SCRIPT_LOC}")
SECTION="EmulationStationBGM"
BACKTITLE="Toolbox"
TITLE="EmulationStation BGM"
AUTOSTART="/opt/retropie/configs/all/autostart.sh"
PYGAME_PKG="python3-pygame"
PSUTIL_PKG="python3-psutil"
OLDIFS=$IFS

function get_pid() {
  echo "$( pgrep -f $(basename ${SCRIPT_LOC}) )"
}
function enable_app() {
  if [ ! -f "${AUTOSTART}" ]; then echo "EmulationStation BGM could not find ${AUTOSTART}"; fi;
  if [[ ! "$(grep -i ${SCRIPT_LOC} ${AUTOSTART})" ]]; then
    sed -i "1s|^|(nohup python3 $SCRIPT_LOC > /dev/null 2>\&1) \&\n&|" $AUTOSTART
    echo "EmulationStation BGM is now enabled."
  fi
}
function disable_app() {
  if [ ! -f "${AUTOSTART}" ]; then echo "EmulationStation BGM could not find ${AUTOSTART}"; fi;
  if [[ "$(grep -i ${SCRIPT_LOC} ${AUTOSTART})" ]]; then
    sed -i "/$(basename $SCRIPT_LOC)/d" $AUTOSTART
    echo "EmulationStation BGM is now disabled."
  fi
}
function start_app() {
  if [ -z $(get_pid) ]; then
    (nohup python3 "${SCRIPT_LOC}" > /dev/null 2>&1) &disown
  fi
}
function stop_app() {
  PID=$(get_pid)
  if [ ! -z $PID ]; then
    (kill $PID > /dev/null 2>&1)
  fi
}
function restart_app() {
  stop_app
  start_app
}

## Use Python because if there is a configuration failure, it's universal.
function read_config() {
  local pyscript="import configparser
config = configparser.ConfigParser()
config.read('$INI_LOC')
section='$SECTION'
gen = ('{}={}'.format(key, config[section][key]) for key in config[section] if section in config.sections())
for pair in gen:
  print(pair, end='|o|')"
  echo "$pyscript" | python3
}
function write_config() {
  local pyscript="import configparser
config = configparser.ConfigParser()
config.read('$INI_LOC')
config['$SECTION']['$1'] = '$2'
with open('$INI_LOC', 'w') as f:
  config.write(f)"
  echo "$pyscript" | python3
}
function remove_config() {
  local pyscript="import configparser
config = configparser.ConfigParser()
config.read('$INI_LOC')
config.remove_section('$SECTION')
with open('$INI_LOC', 'w') as f:
  config.write(f)
"
  echo "$pyscript" | python3
}

function main() {
  local choice
  while true; do
    PKG_STATUS=0
    # Check for installation of script
    if [ -f "$SCRIPT_LOC" ]; then
      PKG_STATUS=1
      RUN_STATE=$(if [ ! -z $(get_pid) ]; then echo "Playing"; else echo "Stopped"; fi;)
      ENABLED=$(if [ "$(cat ${AUTOSTART} 2>&1 | grep $SCRIPT_LOC)" != "" ]; then echo "True"; else echo "False"; fi;)
      config=$(read_config)
      CUR_VOL="$(echo $config | perl -ne 'print "$1" while /max_volume=(.*?)\|o\|/g;' | awk '{print $1 * 100}')"
      STEP_SIZE="$(echo $config | perl -ne 'print "$1" while /vol_step_size=(.*?)\|o\|/g;' | awk '{print $1 * 100}')"
      FADE_DURATION="$(echo $config | perl -ne 'print "$1" while /fade_duration=(.*?)\|o\|/g;')"
      RESUME_DELAY="$(echo $config | perl -ne 'print "$1" while /resume_delay=(.*?)\|o\|/g;')"
      MAINLOOP="$(echo $config | perl -ne 'print "$1" while /main_loop_sleep=(.*?)\|o\|/g;')"
      INIT_DELAY="$(echo $config | perl -ne 'print "$1" while /init_delay=(.*?)\|o\|/g;')"
      RESET="$(echo $config | perl -ne 'print "$1" while /reset_song=(.*?)\|o\|/g;')"
      INIT_SONG="$(echo $config | perl -ne 'print "$1" while /init_song=(.*?)\|o\|/g;')"
      CPU_THRESHOLD="$(echo $config | perl -ne 'print "$1" while /cpu_threshold=(.*?)\|o\|/g;')"
      MUSIC_DIR="$(echo $config | perl -ne 'print "$1" while /music_dir=(.*?)\|o\|/g;')"
      LOGGING="$(echo $config | perl -ne 'print "$1" while /logging=(.*?)\|o\|/g;')"
      MUSIC_DIR="${MUSIC_DIR/#~/$HOME}"
    fi
    cmd=(dialog \
      --backtitle "$BACKTITLE | Music Folder: $MUSIC_DIR" \
      --title "$TITLE" \
      --cancel-label "Exit" \
      --menu "Choose an option" 0 0 0 )
    if [ "$PKG_STATUS" == 1 ]; then
      options=( \
        1 "State: ${RUN_STATE}"
        2 "BGM Volume (${CUR_VOL}%)"
        3 "Volume Step Size (${STEP_SIZE}%)"
        4 "Fade Duration (${FADE_DURATION}ms)"
        5 "Resume Delay (${RESUME_DELAY}ms)"
        6 "Restart on Resume (${RESET})"
        7 "Main loop sleep (${MAINLOOP}ms)"
        8 "Initial Delay (${INIT_DELAY}s)"
        9 "Initial Song ($([ ! -z $INIT_SONG ] && basename ${INIT_SONG}))"
        10 "CPU Threshold (${CPU_THRESHOLD}%)"
        11 "Change Music Folder"
        12 "Start on Boot (${ENABLED})"
        13 "Logging Menu"
        14 "Uninstall ES BGM")
    else
      options=( \
        1 "Install ES BGM")
    fi
    choice=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
    case $choice in
      1) if [ "$PKG_STATUS" == 1 ]; then set_running; else install_package; fi; ;;
      2) set_bgm_volume ;;
      3) set_step_size ;;
      4) set_fade_duration ;;
      5) set_resume_delay ;;
      6) restart_on_resume ;;
      7) set_main_loop_sleep ;;
      8) set_startup_delay ;;
      9) set_start_song ;;
      10) set_cpu_threshold ;;
      11) set_music_dir ;;
      12) set_bgm_enable ;;
      13) logging_menu ;;
      14) uninstall_package ;;
      *) exit ;;
    esac
  done
}

function set_running() {
  if [ -z $(get_pid) ]; then
    start_app
  else
    stop_app
  fi
}
function set_bgm_volume() {
  local NEW_VAL
  NEW_VAL=$(dialog \
    --backtitle "$BACKTITLE" \
    --title "$TITLE" \
    --rangebox "Set volume level (D+/U-): " 0 50 0 100 "$CUR_VOL" \
    2>&1 >/dev/tty)
  if [ -z "$NEW_VAL" ] || [ "$NEW_VAL" == "$CUR_VOL" ]; then return; fi;
  echo "BGM volume set to $NEW_VAL%"
  NEW_VAL=`echo $NEW_VAL | awk '{print $1 / 100}'`
  write_config "max_volume" $NEW_VAL
  restart_app

  sleep $INFO_DELAY
}
function set_step_size() {
  local NEW_VAL
  NEW_VAL=$(dialog \
    --backtitle "$BACKTITLE" \
    --title "$TITLE" \
    --rangebox "Set volume step size in percentage per step (D+/U-): " 0 50 0 100 "$STEP_SIZE" \
    2>&1 >/dev/tty)
  if [ -z "$NEW_VAL" ] || [ "$NEW_VAL" == "$STEP_SIZE" ]; then return; fi;
  echo "BGM volume step size set to ${NEW_VAL}%"
  NEW_VAL=`echo $NEW_VAL | awk '{print $1 / 100}'`
  write_config "vol_step_size" $NEW_VAL
  restart_app
  
  sleep $INFO_DELAY
}
function set_fade_duration() {
  local NEW_VAL
  NEW_VAL=$(dialog \
    --backtitle "$BACKTITLE" \
    --title "$TITLE" \
    --rangebox "Set fade duration in milliseconds (D+/U-): " 0 50 0 10000 "$FADE_DURATION" \
    2>&1 >/dev/tty)
  if [ -z "$NEW_VAL" ] || [ "$NEW_VAL" == "$FADE_DURATION" ]; then return; fi;
  echo "Music fade duration set to ${NEW_VAL}ms"
  write_config "fade_duration" $NEW_VAL
  restart_app
  
  sleep $INFO_DELAY
}
function set_resume_delay() {
  local NEW_VAL
  NEW_VAL=$(dialog \
    --backtitle "$BACKTITLE" \
    --title "$TITLE" \
    --rangebox "Set delay after process end (ex. omxplayer) in milliseconds before restarting music (D+/U-): " 4 50 0 10000 "$RESUME_DELAY" \
    2>&1 >/dev/tty)
  if [ -z "$NEW_VAL" ] || [ "$NEW_VAL" == "$RESUME_DELAY" ]; then return; fi;
  echo "Delay before music restarts set to ${NEW_VAL}ms"
  write_config "resume_delay" $NEW_VAL
  restart_app
  
  sleep $INFO_DELAY
}
function restart_on_resume() {
  if [ "$RESET" == "True" ]; then
    echo "Music will now resume from previous song upon returning to ES."
    write_config "reset_song" "False"
  else
    echo "Music will now restart upon returning to ES."
    write_config "reset_song" "True"
  fi
  restart_app
  
  sleep $INFO_DELAY
}
function set_main_loop_sleep(){
  local NEW_VAL
  NEW_VAL=$(dialog \
    --backtitle "$BACKTITLE" \
    --title "$TITLE" \
    --rangebox "Set main loop sleep in milliseconds (D+/U-): " 0 50 0 1000 "$MAINLOOP" \
    2>&1 >/dev/tty)
  if [ -z "$NEW_VAL" ] || [ "$NEW_VAL" == "$MAINLOOP" ]; then return; fi;
  echo "Main loop sleep duration set to ${NEW_VAL}ms"
  write_config "main_loop_sleep" $NEW_VAL
  restart_app
  
  sleep $INFO_DELAY
}
function set_startup_delay() {
  local NEW_VAL
  NEW_VAL=$(dialog \
    --backtitle "$BACKTITLE" \
    --title "$TITLE" \
    --rangebox "Set initial delay (in seconds) (D+/U-): " 0 50 0 600 "$INIT_DELAY" \
    2>&1 >/dev/tty)
  if [ -z "$NEW_VAL" ] || [ "$NEW_VAL" == "$INIT_DELAY" ]; then return; fi;
  echo "Startup delay set to ${NEW_VAL}s"
  write_config "init_delay" $NEW_VAL
  
  sleep $INFO_DELAY
}
function set_start_song() {
  IFS=$'\n'
  local SELECTION
  local CUR_DIR="$MUSIC_DIR"
  while [ -z "$SELECTION" ]; do
    [[ "${CUR_DIR}" != */ ]] && CUR_DIR="${CUR_DIR}"/
    local cmd=(dialog \
      --backtitle "$BACKTITLE | Current Folder: $CUR_DIR" \
      --title "$TITLE" \
      --menu "Choose a initial song" 20 70 20 )
    local iterator=1
    local offset=-1
    local options=()
    if [ "$(dirname $CUR_DIR)" != "$CUR_DIR" ]; then
      options+=(0)
      options+=("Parent Directory")
      offset=$(($offset+2))
    fi
    options+=(1)
    options+=("<Remove Selection>")
    iterator=$(($iterator+1))
    for DIR in $(find "$CUR_DIR" -maxdepth 1 -mindepth 1 -type d | sort); do
      options+=($iterator)
      options+=("$(basename $DIR)")
      iterator=$(($iterator+1))
    done
    for FILE in "$CUR_DIR"/* ; do
      if [[ $FILE == *.mp3 ]] || [[ $FILE == *.ogg ]]; then
        options+=($iterator)
        options+=($(basename "${FILE}"))
        iterator=$(($iterator+1))
      fi
    done
    choice=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
    case $choice in
      0) CUR_DIR="$(dirname $CUR_DIR)" ;;
      1) SELECTION=''; break ;;
      '') return ;;
      *) [[ -f "$CUR_DIR${options[ $((2*choice + $offset )) ]}" ]] && SELECTION="$CUR_DIR${options[ $((2*choice + $offset )) ]}" || CUR_DIR="$CUR_DIR${options[ $((2*choice + $offset )) ]}" ;;
    esac
  done
  if [ "$SELECTION" != "$INIT_SONG" ]; then
    echo "Initial song changed to '$SELECTION'"
    write_config "init_song" "$SELECTION"
  elif [ "$SELECTION" == "$INIT_SONG" ]; then
    echo "Initial song is already '$SELECTION'"
  fi
  IFS=$OLDIFS
  sleep $INFO_DELAY
}
function set_cpu_threshold() {
  local NEW_VAL
  NEW_VAL=$(dialog \
    --backtitle "$BACKTITLE" \
    --title "$TITLE" \
    --rangebox "Set CPU threshold (D+/U-): " 0 50 0 100 "$CPU_THRESHOLD" \
    2>&1 >/dev/tty)
  if [ -z "$NEW_VAL" ] || [ "$NEW_VAL" == "$CPU_THRESHOLD" ]; then return; fi;
  echo "CPU threshold set to $NEW_VAL%"
  write_config "cpu_threshold" $NEW_VAL
  restart_app

  sleep $INFO_DELAY
}
function set_music_dir() {
  IFS=$'\n'
  local SELECTION
  local CUR_DIR="$MUSIC_DIR"
  while [ -z $SELECTION ]; do
    [[ "${CUR_DIR}" != */ ]] && CUR_DIR="${CUR_DIR}"/
    local cmd=(dialog \
      --backtitle "$BACKTITLE | Current Folder: $CUR_DIR" \
      --title "$TITLE" \
      --menu "Choose a music directory" 20 70 20 )
    local iterator=1
    local offset=-1
    local options=()
    if [ "$(dirname $CUR_DIR)" != "$CUR_DIR" ]; then
      options+=(0)
      options+=("Parent Directory")
      offset=$(($offset+2))
    fi
    options+=($iterator)
    options+=("<Use This Directory>")
    iterator=$(($iterator+1))
    for DIR in $(find "$CUR_DIR" -maxdepth 1 -mindepth 1 -type d | sort); do
      options+=($iterator)
      options+=("$(basename $DIR)")
      iterator=$(($iterator+1))
    done
    choice=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
    case $choice in
      0) CUR_DIR="$(dirname $CUR_DIR)" ;;
      1) SELECTION="$CUR_DIR" ;;
      '') return ;;
      *) CUR_DIR="$CUR_DIR${options[ $((2*choice + $offset )) ]}" ;;
    esac
  done
  [[ "${MUSIC_DIR}" != */ ]] && MUSIC_DIR="${MUSIC_DIR}/"
  if [ "$SELECTION" != "$MUSIC_DIR" ]; then
    echo "Music directory changed to '$SELECTION'"
    write_config "music_dir" "$SELECTION"
    restart_app
  elif [ "$SELECTION" == "$MUSIC_DIR" ]; then
    echo "Music directory is already '$SELECTION'"
  else
    return
  fi
  IFS=$OLDIFS
  sleep $INFO_DELAY
}
function set_bgm_enable() {
  if [ "$(cat $AUTOSTART 2>&1 | grep "$SCRIPT_LOC")" != "" ]; then
    disable_app
  else
    enable_app
  fi
  sleep $INFO_DELAY
}
function logging_menu() {
  local cmd
  while :; do
    cmd=$(dialog \
      --backtitle "$BACKTITLE | Log path: $LOG_LOC" \
      --title "$TITLE" \
      --menu "Choose an option" 0 0 0 \
      1 "Logging (${LOGGING})" \
      2 "View Log" \
      2>&1 >/dev/tty)
    case $cmd in
      1) set_logging ;;
      2) view_logging ;;
      *) return ;;
    esac
  done
}
function set_logging() {
  if [ "$LOGGING" == "True" ]; then
    echo "EmulationStation BGM will stop logging."
    write_config "logging" "False"
    LOGGING="False"
  else
    echo "EmulationStation BGM will start logging."
    write_config "logging" "True"
    LOGGING="True"
  fi
  restart_app
  
  sleep $INFO_DELAY
}
function view_logging() {
  dialog \
    --backtitle "$BACKTITLE | Log path: $LOG_LOC" \
    --title "$TITLE" \
    --tailbox ${LOG_LOC} -1 -1
}
function install_package() {
  DISCLAIMER=""
  DISCLAIMER="${DISCLAIMER}This module installs a python script that depends on \n"
  DISCLAIMER="${DISCLAIMER}$PYGAME_PKG and $PSUTIL_PKG. For your convenience, \n"
  DISCLAIMER="${DISCLAIMER}the following steps will be automated: \n"
  DISCLAIMER="${DISCLAIMER}   - Installation of $PYGAME_PKG and $PSUTIL_PKG \n"
  DISCLAIMER="${DISCLAIMER}   - Downloading EmulationStation BGM from github \n"
  DISCLAIMER="${DISCLAIMER}   - Backup and addition of entries to: \n"
  DISCLAIMER="${DISCLAIMER}       - $AUTOSTART \n"
  DISCLAIMER="${DISCLAIMER}\n\n For more information about EmulationStation BGM, visit \n"
  DISCLAIMER="${DISCLAIMER}'https://gitlab.com/jurassicplayer/emulationstation-bgm'\n"
  dialog \
    --backtitle "$BACKTITLE" \
    --title "DISCLAIMER" \
    --msgbox "$DISCLAIMER" 0 0

  PKG_OK=$(dpkg-query -W --showformat='${Status}\n' $PYGAME_PKG 2>&1 | grep "install ok installed")
  if [ "$PKG_OK" == "" ]; then
    echo "Installing $PYGAME_PKG..."
    sudo apt update && sudo apt install -y $PYGAME_PKG
  fi
  PKG_OK=$(dpkg-query -W --showformat='${Status}\n' $PSUTIL_PKG 2>&1 | grep "install ok installed")
  if [ "$PKG_OK" == "" ]; then
    echo "Installing $PSUTIL_PKG..."
    sudo apt update && sudo apt install -y $PSUTIL_PKG
  fi

  echo "Installing EmulationStation BGM..."
  if [ ! -d "$INSTALL_DIR" ]; then
    sudo mkdir -p "$INSTALL_DIR";
  fi
  sudo chown -R $USER "$INSTALL_DIR"
  if [ -f "$DIR/esbgm.py" ]; then
    cp "$DIR/esbgm.py" "$SCRIPT_LOC"
  else
    wget -O "$SCRIPT_LOC" --progress=bar:force:noscroll --show-progress -q "https://gitlab.com/jurassicplayer/emulationstation-bgm/raw/master/esbgm.py"
  fi
  if [ -f "$DIR/addon.ini" ]; then
    cp "$DIR/addon.ini" "$INI_LOC"
  elif [ ! -f "$INI_LOC" ]; then
    wget -O "$INI_LOC" --progress=bar:force:noscroll --show-progress -q "https://gitlab.com/jurassicplayer/emulationstation-bgm/raw/master/addon.ini"
  fi
  sed -i "s|config_path = \"addon.ini\"|config_path = \"${INI_LOC}\"|" $SCRIPT_LOC
  sed -i "s|log_path = \"esbgm.log\"|log_path = \"${LOG_LOC}\"|" $SCRIPT_LOC
  enable_app
  start_app

  sleep $INFO_DELAY
}
function uninstall_package() {
  local choice
  DISCLAIMER=""
  DISCLAIMER="${DISCLAIMER}This module uninstalls a python script that depends on \n"
  DISCLAIMER="${DISCLAIMER}$PYGAME_PKG and $PSUTIL_PKG. \n\n"
  DISCLAIMER="${DISCLAIMER}In order to not unknowingly break other package dependencies,\n"
  DISCLAIMER="${DISCLAIMER}this dialog is to provide the option of also uninstalling\n"
  DISCLAIMER="${DISCLAIMER}pygame/psutil or leaving them. \n\n"
  DISCLAIMER="${DISCLAIMER}The music folder will not be touched to prevent inadvertently\n"
  DISCLAIMER="${DISCLAIMER}deleting user data."
  dialog \
    --backtitle "$BACKTITLE" \
    --title "DISCLAIMER" \
    --msgbox "$DISCLAIMER" 0 0
  choice=$(dialog \
    --title "Uninstall Packages (Optional)" \
    --ok-label "Continue" \
    --cancel-label "Cancel" \
    --checklist "Uninstall Packages (Optional):" 15 60 15 \
    1 "python3-pygame" OFF \
    2 "python3-psutil" OFF \
    2>&1 >/dev/tty)
    
  if [ $? == 1 ]; then return; fi
  case $choice in
    1 | '1 2') UNINSTALL_PYGAME=1 ;;&
    2 | '1 2') UNINSTALL_PSUTIL=1 ;;
  esac
  disable_app
  stop_app
  if [ "$UNINSTALL_PYGAME" == 1 ]; then
      echo "Uninstalling $PYGAME_PKG..."
      sudo apt-get remove --autoremove -y $PYGAME_PKG
  fi
  if [ "$UNINSTALL_PSUTIL" == 1 ]; then
      echo "Uninstalling $PSUTIL_PKG..."
      sudo apt-get remove --autoremove -y $PSUTIL_PKG
  fi
  echo "Removing EmulationStation BGM script..."
  # Remove script from /opt/esbgm/esbgm.py
  rm "$SCRIPT_LOC"
  if [ -f "$INI_LOC" ]; then
    remove_config
    if [ ! -s "$INI_LOC" ]; then
      echo "Removing EmulationStation BGM addon.ini..."
      rm "$INI_LOC"
    fi
  fi
  sudo rmdir "$INSTALL_DIR"

  sleep $INFO_DELAY
}

main