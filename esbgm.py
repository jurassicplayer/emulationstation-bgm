#!/usr/bin/env python3
write_config = True
config_path = "addon.ini"
log_path = "esbgm.log"
debug = False

# ----------Do not edit below this line--------- #
import os, sys, time, random, configparser, asyncio, logging, psutil

skip_reset_proc_names = ["mplayer","omxplayer","omxplayer.bin","emulationstation"]
proc_names = ["wolf4sdl-3dr-v14", "wolf4sdl-gt-v14", "wolf4sdl-spear", "wolf4sdl-sw-v14", 
    "xvic","xvic cart","xplus4","xpet","x128","x64sc","x64","PPSSPPSDL","prince",
    "OpenBOR","retroarch","ags","uae4all2","uae4arm","capricerpi","linapple",
    "hatari","stella","atari800","xroar","vice","daphne.bin","reicast","pifba","osmose",
    "gpsp","jzintv","basiliskll","mame","advmame","dgen","openmsx","mupen64plus","gngeo",
    "dosbox","ppsspp","simcoupe","scummvm","snes9x","pisnes","frotz","fbzx","fuse","gemrb",
    "cgenesis","zdoom","eduke32","lincity","love","kodi","alephone","micropolis","openbor",
    "openttd","opentyrian","cannonball","tyrquake","ioquake3","residualvm","xrick","sdlpop",
    "uqm","stratagus","wolf4sdl","solarus_run","mplayer","omxplayer","omxplayer.bin",
    "drastic","emulationstation"]

logging_level = logging.DEBUG
if not debug: 
  logging_level = logging.INFO
  logging.disable(logging_level)
logging.basicConfig(filename=log_path, level=logging_level, format='[%(asctime)s] %(name)s:%(levelname)s => %(message)s', datefmt='%H:%M:%S')

class MusicPlayer(object):
  def __init__(self, max_volume, vol_step_size, fade_duration):
    self.log = logging.getLogger("MusicPlayer")
    self.log.debug("Initializing music player")
    if max_volume > 1.0: max_volume = 1.0
    self._mixer_res = 0.0078125                                                       ## Mixer value bucketing
    self._max_volume = int(max_volume / self._mixer_res) * self._mixer_res            ## Mixer value bucketing
    self._vol_step_size = int(vol_step_size / self._mixer_res) * self._mixer_res      ## Mixer value bucketing
    if self._vol_step_size < self._mixer_res: self._vol_step_size = self._mixer_res   ## Mixer value bucketing
    self._fade_duration = fade_duration
    self._step_duration = self._fade_duration / (self._max_volume / self._vol_step_size)
    self._fade_active = False
    self._volume_state = 0
    self._player_status = 0
    self._current_song = ""
    self._current_pos = 0

    from pygame import mixer
    mixer.init()
    self.mixer = mixer.music
    self.mixer.set_volume(0)

  def play(self, song_path=None, reset=False):
    if song_path:
      self._current_song = song_path
      self._current_pos = 0
      self.mixer.load(self._current_song)
    if reset:
      self._current_pos = 0
    self._volume_state = 0
    self.mixer.set_volume(0)
    self.log.info("Playing song: {} at pos {}".format(self._current_song, self._current_pos))
    if '.mp3' in self._current_song:
      self.log.info("If the log stops here or there is no music,\n\t\t\t\t\t\t{} is causing pygame troubles.".format(os.path.basename(self._current_song)))
    self.mixer.play(start=self._current_pos)
    self.volume_state = self._max_volume

  def stop(self):
    self.log.info("Stopping song: {} at pos {}".format(self._current_song, self._current_pos))
    self._current_pos += self.mixer.get_pos() / 1000
    self.mixer.stop()

  async def fade(self):
    if self._fade_active: return
    self._fade_active = True
    self.log.debug("Async fade started")
    start = time.time()
    while self._fade_active:
      current_vol = self.mixer.get_volume()
      if current_vol == self.volume_state:
        self._fade_active = False
        self.log.debug("Total fade duration: {}".format(time.time() - start))
        continue
      if current_vol > self.volume_state:
        current_vol -= self._vol_step_size
        if current_vol < 0: current_vol = 0
      elif current_vol < self.volume_state:
        current_vol += self._vol_step_size
        if current_vol > self.volume_state: current_vol = self.volume_state
      self.mixer.set_volume(current_vol)
      await asyncio.sleep(self._step_duration)
    if current_vol == 0:
      self.stop()

  @property
  def volume_state(self):
    return self._volume_state
  @volume_state.setter
  def volume_state(self, volume):
    volume = int(volume / self._mixer_res) * self._mixer_res
    self.log.debug("Changing volume_state state: {} --> {}".format(self.volume_state, volume))
    self._volume_state = volume

    if sys.version_info < (3, 8, 0): ## Multi-version support since latest version of Python on Raspbian is 3.5.3
      loop = asyncio.get_event_loop()
      loop.create_task(self.fade())
    else:
      asyncio.create_task(self.fade())
  @property
  def player_status(self):
    try:
      status = self.mixer.get_busy()
    except:
      status = False
    return status
  @property
  def volume_status(self):
    try:
      status = self.mixer.get_volume()
    except:
      status = 0
    return status
  @property
  def current_song(self):
    return self._current_song

class Application:
  def __init__(self, config_path):
    self.log = logging.getLogger("Application")
    self.log.debug("Initializing...")
    self.process_names = proc_names
    self.write_config = write_config
    self.process_countdown = 0
    self.resetter = False
    self.alive = self.load_config(config_path)
    self.log.debug("Initialization Complete")
  
  def load_config(self, config_path):
    cfg_name = "EmulationStationBGM"
    config = configparser.ConfigParser()
    config[cfg_name] = {
      'music_dir'       : '~/RetroPie/music',
      'init_song'       : '',
      'init_delay'      : '0',
      'main_loop_sleep' : '1000',
      'max_volume'      : '0.20',
      'vol_step_size'   : '0.01',
      'fade_duration'   : '3000',
      'resume_delay'    : '3000',
      'reset_song'      : 'False',
      'cpu_threshold'   : '70',
      'logging'         : 'False',
    }
    config_path = os.path.realpath(os.path.expanduser(config_path))
    self.log.debug("config_path: {}".format(config_path))
    
    previous_cfg = False
    if os.path.isfile(config_path):
      with open(config_path) as f:
        data = f.readlines()
        for line in data:
          if "[{}]".format(cfg_name) in line:
            previous_cfg = True
            break
      self.log.debug("Reading from config_path")
      config.read(config_path)
    if self.write_config and not previous_cfg:
      self.log.debug("Writing to config_path")
      with open(config_path, 'w') as f:
        config.write(f)

    try:
      logging_enabled = config[cfg_name].getboolean("logging")
    except ValueError:
      logging_enabled = True
    if logging_enabled:
      logging.disable(logging.NOTSET)
      self.log.info("######################################")
      self.log.info("Logging enabled: {}".format(log_path))
    try:
      self.music_dir        = os.path.realpath(os.path.expanduser(config[cfg_name].get("music_dir")))
      self.library          = []
      self.init_song        = config[cfg_name].get("init_song")

      key = "music_dir"
      if not os.path.isdir(self.music_dir) or not os.path.exists(self.music_dir):
        raise ValueError("{} not found".format(self.music_dir))
      for filename in os.listdir(self.music_dir):
        if os.path.isfile(os.path.join(self.music_dir, filename)) and os.path.splitext(filename)[1] in ['.ogg', '.mp3']:
          self.library.append(os.path.join(self.music_dir, filename))
      if not self.library:
        raise ValueError("Music not found (.ogg/.mp3)")
      key = "init_song"
      init_song_path = os.path.realpath(os.path.expanduser(self.init_song))
      if self.init_song and not os.path.isfile(init_song_path):
        raise ValueError("{} not found".format(init_song_path))

      key = "init_delay"
      self.init_delay       = config[cfg_name].getint("init_delay")
      key = "main_loop_sleep"
      self.main_loop_sleep  = config[cfg_name].getint("main_loop_sleep") / 1000
      key = "max_volume"
      max_volume            = config[cfg_name].getfloat("max_volume")
      key = "vol_step_size"
      vol_step_size         = config[cfg_name].getfloat("vol_step_size")
      key = "fade_duration"
      fade_duration         = config[cfg_name].getint("fade_duration") / 1000
      key = "resume_delay"
      self.resume_delay     = config[cfg_name].getint("resume_delay") / 1000
      key = "reset_song"
      self.reset_song       = config[cfg_name].getboolean("reset_song")
      key = "cpu_threshold"
      self.cpu_threshold    = config[cfg_name].getint("cpu_threshold")
    except ValueError as e:
      self.log.warning("Invalid config '{}': {}".format(key, e))
      return False

    self.log.debug('# Configuration #')
    for key in config[cfg_name]:
      self.log.debug("{} = {}".format(key, config[cfg_name][key]))
    self.log.debug("library = {}".format(self.library))
    self.log.debug("#################\n")
    self.mp = MusicPlayer(max_volume, vol_step_size, fade_duration)
    return True

  def monitor_process(self):
    for proc in psutil.process_iter(['pid', 'name']):
      p = proc.info
      if p['name'] in self.process_names:
        if p['name'] in 'emulationstation':
          p_cpu = psutil.Process(p['pid']).cpu_percent(0.2)
          if p_cpu < self.cpu_threshold: continue
          self.log.debug("CPU Threshold passed {}".format(self.cpu_threshold, p_cpu))
        if p['name'] not in skip_reset_proc_names and self.reset_song:
          self.resetter = True
        else:
          self.resetter = False
        if self.mp.volume_state:
          self.mp.volume_state = 0
        self.process_countdown = self.resume_delay
        return
    self.log.debug("No flagged process found")
    if not self.mp.volume_state: 
      self.log.debug("Player is currently muted")
      if self.process_countdown <= 0:
        self.mp.play(reset=self.resetter)
        self.resetter=False
      elif self.process_countdown > 0:
        self.process_countdown -= self.main_loop_sleep
    elif self.mp.volume_state and not self.mp.player_status:
      self.log.debug("Player is currently idle")
      next_song = self.mp.current_song
      if len(self.library) > 1:
        while next_song == self.mp.current_song:
          next_song = random.choice(self.library)
      self.mp.play(next_song)

  def run(self):
    if self.alive:
      if sys.version_info < (3, 8, 0): ## Multi-version support since latest version of Python on Raspbian is 3.5.3
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.mainloop())
        loop.close()
      else:
        asyncio.run(self.mainloop())

  async def mainloop(self):
    if self.init_delay:
      self.log.info("Init sleep for {}s".format(self.init_delay))
      await asyncio.sleep(self.init_delay)
    if self.init_song:
      self.mp.play(os.path.realpath(os.path.expanduser(self.init_song)))
    else:
      self.mp.play(random.choice(self.library))
    while self.alive:
      self.log.debug("volume_state: {}  mixer_volume: {}  player_status: {}  process_countdown: {}".format(self.mp.volume_state, self.mp.volume_status, self.mp.player_status, self.process_countdown))
      self.monitor_process()
      await asyncio.sleep(self.main_loop_sleep)

if __name__ == "__main__":
  try:
    app = Application(config_path)
    app.run()
  except KeyboardInterrupt:
    print('')
